#!/bin/bash -v

#instance ID
instance=$USER

# Database credentials
user="$instance_live"
password="agp5wr5bCUnFUaVa"
host="localhost"
db_name="$instance_live"

# Other options
backup_path="/home/$instance/www/wp-content/database"
date=$(date +"%d-%b-%Y")

# Set default file permissions
umask 177

# Dump database into SQL file
 mysqldump --user=$user --password=$password --host=$host $db_name > $backup_path/$db_name-$date.sql

# Git: add and commit changes
cd /home/$instance/www/wp-content && /usr/bin/git commit -a -m "daily crontab backup `date`"

# send data to Git server
cd /home/$instance/www/wp-content && /usr/bin/git push origin master
