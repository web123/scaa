<?php 

get_header();



    global $enhanced_category;
    // if not previously set up, then let setup_ec_data get the current query term/category
    if (empty($categoryId)) {
        $categoryId = null;
    }

    // get enhanced category post and set it up as global current post
    $enhanced_category->setup_ec_data($categoryId);
?>

<!-- enchanced category page (ECP) content -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-thumbnail">
	  <?php the_post_thumbnail(); ?>
    </div>

    <div class="entry-content">
        <?php the_content(); ?>
    </div><!-- .entry-content -->

    <?php edit_post_link( __( 'Edit'), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->

<?php get_footer(); ?>	