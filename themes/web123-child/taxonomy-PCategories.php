<?php
/**
* Template Name: PCategories
*
*/
get_header();
	?>	





				<div class="container">
	 
					<div class="container_inner default_template_holder">
						<div class="blog_single blog_holder">			
							<div class="post_content_holder">
							  
<?php 
$category = get_queried_object();
$id= $category->term_id;

if (!function_exists(‘get_all_wp_terms_meta’))
{
  // echo "gotit";
$arrayMetaList = get_all_wp_terms_meta($id);
  // echo "<pre>"; print_r($arrayMetaList);
   //echo "<pre>"; print_r($arrayMetaList); echo "</pre>";
	$newarray = array();
  foreach ($arrayMetaList as $key => $value) {

	//echo 'for '.$key.' value is '.$value[0].'<br>';

	$newarray[$key] = $value[0];

//echo "<pre>";
	/*print_r($key);
	print_r($arrayMetaList[$key]);
	print_r($value);*/	
	//echo "<pre>"; print_r($value);
	//echo "<br/>";
	//echo $value->
}
	?>
<?php   // Get terms for post
 $terms = get_the_terms( $post->ID , 'PCategories' );
 // Loop over each item since it's an array
 if ( $terms != null ){
 foreach( $terms as $term ) {
 // Print the name method from $term which is an OBJECT
?>

<h1 class="cate_name"><?php echo $term->slug ; ?></h1>
<?php
 unset($term);
} } ?>
							  
							  <div class="cate_name"><?php echo $newarray['category-slogan']; ?></div>
							  
							  <div class=""><?php  echo category_description(); ?></div>
							
							  <div class="layout-design">
							  <div class="row-layout-title">
							  
							  <div class="H-title">
							  <div class="heading"><?php  echo $newarray['title-1']; ?></div>
							  <?php  echo $newarray['assessment']; ?>
							  </div>
								
							 <div class="H-title">
							  <div class="heading"><?php  echo $newarray['title-2']; ?></div>
							  <?php  echo $newarray['offer']; ?>
							  </div>
								
							 <div class="H-title">
							  <div class="heading"><?php  echo $newarray['title-3']; ?></div>
							  <?php  echo $newarray['features']; ?>
							  </div>
							  
							  </div>
								<div class="last-text-paregraph">
							  <?php  echo $newarray['last-text']; ?>	
								</div>
							  </div>
							  
							  
							  
							  
							  
							  
            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array('post_type' => 'newss', 'posts_per_page' => 6, 'paged' => $paged);
            $mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
            foreach ($posts as $post) {
                ?>
<div class="news-detail">
  
  <div class="">News</div>

<div class="news-description">
 <h2 class="handing"><a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a></h2>
  <div class="date-off-post">
 <?php $post_date = get_the_date( 'l j F, Y' ); 
			  
			  echo $post_date;?>
  </div>
 <p class="news-content"><?php echo substr($post->post_content, 0, 200); ?></p></div>
              
                

</div>
            <?php }
            ?>
  
							  
							  
							  
	
            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array('post_type' => 'products', 'posts_per_page' => 6, 'paged' => $paged);
            $mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
            foreach ($posts as $post) {
                ?>
<div class="related-detail">
  
  <div class="">Product</div>
<div class="related-product-image">
                <?php the_post_thumbnail(); ?>
</div>

<div class="related-product-content"> <h2 class="handing"><a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a></h2>
 <p><?php echo substr($post->post_content, 0, 100); ?></p></div>
              
                

</div>
            <?php }
            ?>
  
  						  
							  

<?php
  
  

  // echo $newarray['title-1'];
  //  echo '<br/>';
  // echo $newarray['title-2'];
  //    echo '<br/>';
  //  echo $newarray['title-3'];
  //echo '<br/>';
 
  // echo $newarray['news-post-shortcode'];
  // echo '<br/>';
  //echo $newarray['product-shortcode'];
  //echo '<br/>';
   
}



?>
							  
							  
							</div>								
						</div>
					</div>
				</div>
			
	<?php	
 get_footer(); ?>