<?php
    /*Template Name: single post template
    */
    
    get_header(); ?>
<?php
    $mypost = array( 'post_type' => 'products', );
    $loop = new WP_Query( $mypost );
    //print_r($loop);
    
    global $post;
    ?>


						
<div class="container">
  <div class="container_inner product-margin">
     <div class="four_columns clearfix">
         <div class="post_content_holder">
             <div class="post_text">
                <div class="post_text_inner">
                      <h2 itemprop="name" class="entry_title"><?php echo  $post->post_title ?></h2>
                         <div class="row baredcurmb">
                             <?php custom_breadcrumbs(); ?>

                          </div><!--row baredcurmb-->

                          <div class="Brand-images-product">

                             <?php 

                                 $image = get_field('brand_images');

                                    if( !empty($image) ): ?>

	                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                              <?php endif; ?>

                          </div><!--Brand-images-product-->

                          <div class="Brand-content-product">
                                <?php echo $post->post_content; ?>
                                    <div class="post_customfields product">
									  
									  <?php 

 if(get_field('website_link'))
{
  
  ?>
  
                                          <a href="<?php the_field('website_link'); ?>" class="link-btn link-btn-default"><?php the_field('website_button_text'); ?></a>
									  
									  <?php
  
}else
{

}
  ?>
									  
									  
						 <?php 

 if(get_field('video_link'))
{
  
  ?>			  
                                              
                                               <a href="<?php the_field('video_link'); ?>" class="link-btn link-btn-default"><?php the_field('video_button_text'); ?></a>
									  
									  <?php
  
}else
{

}
  ?>
									  
									  <?php 

 if(get_field('tour_link'))
{
  
  ?>			  
                          

                                                 <a href="<?php the_field('tour_link'); ?>" class="link-btn link-btn-default"><?php the_field('tour_button_text'); ?></a>
									  
									  <?php
  
}else
{

}
  ?>
									


                                    </div><!--post_customfields-->  
                          </div><!--Brand-content-product-->	

                                    <div class="Brand-thumb-product">
                                                <?php if ( has_post_thumbnail() ) : ?>
                                                 <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                      <?php the_post_thumbnail(); ?>
                                                 </a>
                                                  <?php endif; ?>
                                    </div><!--Brand-thumb-product-->

<div class="post-navigation">
<?php if ($mypost->max_num_pages > 1) { // check if the max number of pages is greater than 1   ?>
                <nav class="prev-next-posts">
                    <?php
                    $a = get_next_posts_link('MORE NEWS', $mypost->max_num_pages);
                    if (!empty($a)) {
                        ?>
                        <div class="prev-posts-link col-md-3" style="text-align-right">
                            <?php echo get_next_posts_link('MORE NEWS', $mypost->max_num_pages); // display older posts link  ?>
                        </div>
                        <?php
                    }
                    $bb = get_previous_posts_link('Prev');
                    if (!empty($bb)) {
                        ?>
                        <div class="next-posts-link col-md-3" style="text-align-left ;  "> 
                            <?php echo get_previous_posts_link('BACK NEWS'); // display newer posts link ?>
                        </div>
                    <?php } ?>
                </nav>
            <?php } ?>

</div>

<div class="related-product-row">
<?php   // Get terms for post
 $terms = get_the_terms( $post->ID , 'PCategories' );
 // Loop over each item since it's an array
 if ( $terms != null ){
 foreach( $terms as $term ) {
 // Print the name method from $term which is an OBJECT
?>

<h1 class="cate_name"><?php echo $term->slug ; ?></h1>
<?php
// echo $term->slug ;
 // Get rid of the other data stored in the object, since it's not needed
 unset($term);
} } ?>




            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array('post_type' => 'products', 'posts_per_page' => 6, 'paged' => $paged);
            $mypost = new WP_Query($args);
            global $post;
            $posts = $mypost->get_posts();
            foreach ($posts as $post) {
                ?>
<div class="related-detail">
<div class="related-product-image">
                <?php the_post_thumbnail(); ?>
</div>

<div class="related-product-content"> <h2 class="handing"><a href="<?php echo get_post_permalink($post->ID); ?>"><?php echo $post->post_title ?></a></h2>
 <p><?php echo substr($post->post_content, 0, 100); ?></p></div>
              
                

</div>
            <?php }
            ?>
  
  
  
  
  <h1>Product</h1>
   		</div>									
					         </div><!--post_text_inner-->		
				       </div><!--post_text-->	
			    </div><!--post_content_holder-->
     </div>	<!--four_columns clearfix -->	



   </div><!--container_inner-->
</div><!--container-->




<?php wp_reset_query(); ?>

<?php get_footer(); ?>	