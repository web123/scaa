<?php
   /*
   Template Name: News Demo Template
   Plugin URL: http://wp.tutsplus.com/
   Description: Loads a custom template file instead of the default single.php
   Version: 0.1
   Author: Remi Corson
   Author URI: http://wp.tutsplus.com/
   */
   
   
    get_header(); ?>
<div id="primary">
   <div id="content" role="main">
      <?php
         $args=array('post_type'=> 'newss');
         $mypost=new WP_Query($args);
         //  print_r($mypost);
         
         global $post;
         
         
         $posts=$mypost->get_posts(); 
         
         
         
         $content ='';
         foreach($posts as $post)
         {
         
         
         // print_r($catr);
         $content .='<div class="movie-block">';
         
         $content .='<div class="col-sm-4 img" style="background-image:url('.get_the_post_thumbnail_url($post->ID, 'full').')">';
         //$content .=get_the_post_thumbnail_url($post->ID, 'thumbnail'); 
         $content .='</div>';
         $content .='<div class="col-sm-8">';
         $content .= '<h2 style="margin:0px;border-bottom: 1px solid;color: #fff;">'.$post->post_title.'</h2>';
         $content.='<p>'.substr($post->post_content, 0, 500).'... </p>';
         $content.='<a type="button" class="btn btn-success" href="'.get_post_permalink($post->ID).'">Read More</a>';
         $content .='</div>';
         $content .='<div class="col-sm-12">';
         $content .='<div class="col-sm-4">';
         $content .='<div class="head">Movie Director</div>';
         $content .=get_post_meta( $post->ID, 'movie_director', true );  
         
         $content .='</div>';
         $content .='<div class="col-sm-4">';
         $content .='<div class="head">Movie Actor</div>';
         $content .='<div>';    
         $content .=get_post_meta( $post->ID, 'movie_actor', true );
         $content .='</div>';
         
         $content .='</div>';
         $content .='<div class="col-sm-4">';
         $content .='<div>';
         $content .='<div class="head">Movie Rating</div>';
         $rate.=get_post_meta($post->ID, 'movie_rating', true);
         
         $content .='<div class="rating1">';
                 
                 $nb_stars = intval( get_post_meta( get_the_ID(), 'movie_rating', true ) );
                 for ( $star_counter = 1; $star_counter <=5 ; $star_counter++ ) {
                     if ( $star_counter <= $rate ) {
                          $content .='<img src="' . plugins_url( 'Movie-Reviews/images/grey.png' ) . '" />';
                     } else {
                         $content .='<img src="' . plugins_url( 'Movie-Reviews/images/icon.png' ). '" />';
                     }
                 }
             
             $content.='</div>';
         $content .='</div>';
         
         $content .='</div>';
         $content .='<div class="col-sm-8 category-continer" style="float:left;">';
         $content .='categories :- ';
         $content .=get_category_by_post_id($post->ID);  
         /*$cat=get_categories($post->ID);
         //$content .=$cat;
         //print_r($cat);
         foreach($cat as $c) {
         $content .=$c->cat_name;
         }*/
         
         
         $content.='</div>';    
         $content .='<div class="col-sm-4"><a href="javascript:void(0)" class="likethis" data-movie_id="'.$post->ID.'" data-user_id="'.get_current_user_id().'">Like</a></div>'; 
         $content .='</div>';
         $content .='</div>';     
         $content .='</div>';
         
         }
         echo $content;
         
         ?>
   </div>
</div>
<?php 
   function get_category_by_post_id($post_id){
   
     $catr=wp_get_post_terms( $post_id,'Categories');
     $category='';
    
     foreach($catr as $cat)
     {
       $link=get_term_link($cat->term_id);
     
       $category .='<a href="'.$link.'">'.$cat->name.'</a>';
     
       $category .=',';
     
       
     }
     $category=substr($category,0,-1);
     return $category;
   }
   
   
   ?>
<?php get_footer(); ?>