<?php /* Template Name: News Pages */ ?>

<?php get_header(); ?>

	<div class="container">
	 
					<div class="container_inner default_template_holder">
						<div class="blog_single blog_holder">			
							<div class="post_content_holder">
							   
<div class="news-detail">
  
  <div class="page-title">
	<h1 class="title-page"><?php the_title(); ?></h1>
  </div>
<div class="news-description">
   
 <?php
			  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array('post_type' => 'newss', 'posts_per_page' => -1, 'paged' => $paged);
	  
			  $loop = new WP_Query($args);
global $post;
$posts=$loop->get_posts(); 
         $content ='';
        if($loop->have_posts()) {
	      $content ='';
	    
	   echo $meta = get_post_meta($post_id);
        echo '<h2>'.$custom_term->name.'</h2>';

        while($loop->have_posts()) : $loop->the_post();
	      echo "<br/>";
	   echo '<h2 class="title-post"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>';
	  
	   $post_date = get_the_date( 'l j F, Y' ); 
		  
		  echo $post_date; echo '<span class="spaceing">by</span>'; echo '<span class="spaceing">'. the_author() .'</span>';
	   
      $content = get_category_by_post_id($post->ID);  
     
	   echo '<span class="spaceing">//</span>';
         echo $content;
			  
			 
	   echo the_content();
	   if(get_field('button_link'))
{
  
  ?>
  <div class="button-text-area"><a href="<?php the_field('button_link'); ?>" class="link-btn link-btn-default"><?php the_field('button_title'); ?></a></div>
  
  <?php
  
}else
{

}
  ?>
  
 
  <?php
	   
        endwhile;
     }
	  ?>
 
 
</div>
</div>
		  </div>
						   </div>
					   </div>
	   </div> 

<?php 
   function get_category_by_post_id($post_id){
   
     $catr=wp_get_post_terms( $post_id,'Categories');
     $category='';
    
     foreach($catr as $cat)
     {
       $link=get_term_link($cat->term_id);
     
       $category .='<a href="'.$link.'">'.$cat->name.'</a>';
     
       $category .=',';
     
       
     }
     $category=substr($category,0,-1);
     return $category;
   }
   
   
   ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

