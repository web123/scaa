<?php /* Template Name: News backup  */ ?>

<?php get_header(); ?>
	<div class="container">
	 
					<div class="container_inner default_template_holder">
						<div class="blog_single blog_holder">			
							<div class="post_content_holder">
							
<div class="news-detail">
  
  <div class="page-title">
	<h1 class="title-page"><?php the_title(); ?></h1>
  </div>


<div class="news-description">
  
 <?php $meta = get_post_meta($post_id);

//echo  $cat = get_category_by_post_id($post->ID); 

 ?> 
  
 <?php
			  $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array('post_type' => 'newss', 'posts_per_page' => -1, 'paged' => $paged);


			  
			  $loop = new WP_Query($args);
     if($loop->have_posts()) {
	    
	   echo $meta = get_post_meta($post_id);
        echo '<h2>'.$custom_term->name.'</h2>';

        while($loop->have_posts()) : $loop->the_post();
	      echo "<br/>";
	   echo '<h2 class="title-post"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>';
	  
	   $post_date = get_the_date( 'l j F, Y' ); 
			  
			  echo $post_date; echo "by"; echo the_author(); 
	   echo the_content();
	   if(get_field('button_link'))
{
  
  ?>
  <?php echo get_the_category(); ?>
  
  <div class="button-text-area"><a href="<?php the_field('button_link'); ?>" class="link-btn link-btn-default"><?php the_field('button_title'); ?></a></div>
  
  <?php
  
}else
{

}
  ?>
  
 
  <?php
	   
        endwhile;
     }
			  ?>
  
  

  
</div>
              
                

</div>
							  
							  
					
							  
						  </div>
						   </div>
					   </div>
	   </div> 



  
		    <?php
         $args=array('post_type'=> 'newss');
         $mypost=new WP_Query($args);
         //  print_r($mypost);
         
         global $post;
         $posts=$mypost->get_posts(); 
         $content ='';
         foreach($posts as $post)
         {
         
         
         // print_r($catr);
         $content .='<div class="movie-block">';
         $content .='<div class="col-sm-4 img" style="background-image:url('.get_the_post_thumbnail_url($post->ID, 'full').')">';
         $content .='</div>';
         $content .='<div class="col-sm-8">';
         $content .= '<h2 style="margin:0px;border-bottom: 1px solid;color: #fff;">'.$post->post_title.'</h2>';
         $content.='<p>'.substr($post->post_content, 0, 500).'... </p>';
         $content.='<a type="button" class="btn btn-success" href="'.get_post_permalink($post->ID).'">Read More</a>';
         $content .='</div>';
         $content .='<div class="col-sm-12">';
         $content .='<div class="col-sm-4">';
         $content .='<div class="head">Movie Director</div>';
         $content .=get_post_meta( $post->ID, 'movie_director', true );  
         
         $content .='</div>';
         $content .='<div class="col-sm-4">';
         $content .='<div class="head">Movie Actor</div>';
         $content .='<div>';    
         $content .=get_post_meta( $post->ID, 'movie_actor', true );
         $content .='</div>';
         
         $content .='</div>';
         $content .='<div class="col-sm-4">';
         $content .='<div>';
         $content .='<div class="head">Movie Rating</div>';
         $rate.=get_post_meta($post->ID, 'movie_rating', true);
         
         $content .='<div class="rating1">';
                 
                 $nb_stars = intval( get_post_meta( get_the_ID(), 'movie_rating', true ) );
                 for ( $star_counter = 1; $star_counter <=5 ; $star_counter++ ) {
                     if ( $star_counter <= $rate ) {
                          $content .='<img src="' . plugins_url( 'Movie-Reviews/images/grey.png' ) . '" />';
                     } else {
                         $content .='<img src="' . plugins_url( 'Movie-Reviews/images/icon.png' ). '" />';
                     }
                 }
             
             $content.='</div>';
         $content .='</div>';
         
         $content .='</div>';
         $content .='<div class="col-sm-8 category-continer" style="float:left;">';
         $content .='categories :- ';
         $content .=get_category_by_post_id($post->ID);  
         /*$cat=get_categories($post->ID);
         //$content .=$cat;
         //print_r($cat);
         foreach($cat as $c) {
         $content .=$c->cat_name;
         }*/
         
         
         $content.='</div>';    
         $content .='<div class="col-sm-4"><a href="javascript:void(0)" class="likethis" data-movie_id="'.$post->ID.'" data-user_id="'.get_current_user_id().'">Like</a></div>'; 
         $content .='</div>';
         $content .='</div>';     
         $content .='</div>';
         
         }
         echo $content;
         
         ?>

<?php 
   function get_category_by_post_id($post_id){
   
     $catr=wp_get_post_terms( $post_id,'Categories');
     $category='';
    
     foreach($catr as $cat)
     {
       $link=get_term_link($cat->term_id);
     
       $category .='<a href="'.$link.'">'.$cat->name.'</a>';
     
       $category .=',';
     
       
     }
     $category=substr($category,0,-1);
     return $category;
   }
   
   
   ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

